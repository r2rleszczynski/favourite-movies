//
//  MovieDetailsViewModel.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 23/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MovieDetailsViewModel {
    
    // MARK: - Input
    let movie: Driver<Movie>
    let newMovieLike: PublishRelay<(Int, Bool)>
    var movieLikes: Driver<Set<Int>>
    
    init(movie: Driver<Movie>,
         newMovieLike: PublishRelay<(Int, Bool)>,
         movieLikes: Driver<Set<Int>>,
         apiType: MovieDBAPIProtocol.Type = MovieDBAPI.self) {
        
        self.movie = movie
        self.newMovieLike = newMovieLike
        self.movieLikes = movieLikes
    }
}
