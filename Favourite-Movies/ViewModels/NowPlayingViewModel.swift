//
//  File.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 22/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt
import RxCocoa

class NowPlayingViewModel {
    private let bag = DisposeBag()
    private let apiType: MovieDBAPIProtocol.Type
    
    private static let moviesLikeFileName = "movieLikes.json"
    
    // MARK: - Input
    let nextPageTrigger: PublishRelay<Void>
    let refreshTrigger: PublishRelay<Void>
    let newMovieLike: PublishRelay<(Int, Bool)>
    let searchText: PublishRelay<String>
    
    // MARK: - Output
    var imageConfig: Driver<ImageConfiguration>!
    var moviesList: BehaviorRelay<ListResponse<Movie>>!
    var activity: PublishRelay<Bool>!
    var movieLikes: BehaviorRelay<Set<Int>>!
    var searchResult: BehaviorRelay<ListResponse<Movie>>!

    init(apiType: MovieDBAPIProtocol.Type = MovieDBAPI.self) {
        self.apiType = apiType
        nextPageTrigger = PublishRelay()
        refreshTrigger = PublishRelay()
        newMovieLike = PublishRelay()
        searchText = PublishRelay()
        
        bindOutput()
    }
    
    private func bindOutput() {
        bindConfig()
        bindSearch()
        let newMovies = bindMovies()
        bindTriggers(newMovies: newMovies)
        bindMovieLikes()
    }
    
    private func bindSearch() {
        searchResult = BehaviorRelay(value: .empty)
        
        let searchMovie: (_ query: String, _ page: Int?) -> Observable<ListResponse<Movie>> = apiType.searchMovie
        searchText
            .filter { !$0.isEmpty }
            .flatMapLatest { text in
                return searchMovie(text, nil).observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            }
            .catchErrorJustReturn(.empty)
            .map { list in
                let results = [Movie](list.results.prefix(20))
                return ListResponse<Movie>(page: 1, results: results, totalPages: 1, totalResults: results.count)
            }
            .bind(to: searchResult)
            .disposed(by: bag)
        
        searchText
            .filter { $0.isEmpty }
            .map { _ in .empty }
            .bind(to: searchResult)
            .disposed(by: bag)
    }
    
    private func bindConfig() {
        imageConfig = apiType.imageConfig()
            .retry(RepeatBehavior.delayed(maxCount: 4, time: 1.0))
            .asDriver(onErrorJustReturn: ImageConfiguration.default)
    }
    
    private func bindMovies() -> Observable<ListResponse<Movie>> {
        moviesList = BehaviorRelay(value: .empty)

        //reference to static function
        let nowPlaying: (_ page: Int?) -> Observable<ListResponse<Movie>> = apiType.nowPlaying
        
        let newMovies = nextPageTrigger
            .asObservable()
            .startWith(())
            .withLatestFrom(moviesList)
            .flatMapLatest { currentMovieList -> Observable<ListResponse<Movie>> in
                let nextPage = currentMovieList.page + 1
                if currentMovieList.totalPages == 0 || nextPage <= currentMovieList.totalPages {
                    return nowPlaying(nextPage).observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                } else {
                    return .empty()
                }
            }
            .scan(ListResponse<Movie>.empty) { total, now in
                // clear list on refresh
                let results = now.page > 1 ? total.results + now.results : now.results
                return ListResponse<Movie>(page: now.page, results: results, totalPages: now.totalPages, totalResults: now.totalResults)
            }
            .share(replay: 1)

        Observable
            .combineLatest(
                newMovies,
                searchResult.asObservable(),
                imageConfig.asObservable()
            )
            .map { nowPlayingList, searchResult, config -> ListResponse<Movie> in
                let movieList = searchResult.isEmpty ? nowPlayingList : searchResult
                let results = movieList.results.map { movie -> Movie in
                    var movie = movie
                    if let posterPath = movie.posterPath {
                        let posterSizes = config.posterSizes
                        movie.posterUrl = config.secureBaseUrl
                            .appendingPathComponent(posterSizes.first { $0 == "w92" } ?? "original")
                            .appendingPathComponent(posterPath)
                        movie.bigPosterUrl = config.secureBaseUrl
                            .appendingPathComponent(posterSizes.first { $0 == "w500" } ?? "original")
                            .appendingPathComponent(posterPath)
                    }
                    return movie
                }
                return ListResponse<Movie>(
                    page: movieList.page,
                    results: results,
                    totalPages: movieList.totalPages,
                    totalResults: movieList.totalResults)
            }
            .catchError { [weak self] _ -> Observable<ListResponse<Movie>> in
                return Observable.just(self?.moviesList.value ?? .empty)
            }
            .observeOn(MainScheduler.instance)
            .bind(to: moviesList)
            .disposed(by: bag)

        return newMovies
    }
    
    private func bindTriggers(newMovies: Observable<ListResponse<Movie>>) {
        refreshTrigger
            .map { ListResponse<Movie>.empty }
            .bind(to: moviesList)
            .disposed(by: bag)
        refreshTrigger
            .bind(to: nextPageTrigger)
            .disposed(by: bag)
        
        activity = PublishRelay<Bool>()
        refreshTrigger
            .map { _ in true }
            .bind(to: activity)
            .disposed(by: bag)
        
        newMovies
            .filter { !$0.isEmpty }
            .map { _ in false }
            .catchErrorJustReturn(false)
            .bind(to: activity)
            .disposed(by: bag)
    }
    
    private func bindMovieLikes() {
        movieLikes = BehaviorRelay<Set<Int>>(value: [])
        newMovieLike
            .subscribe(onNext: { [weak self] id, isOn in
                guard let self = self else { return }
                
                var likes = self.movieLikes.value
                if isOn {
                    likes.insert(id)
                } else  {
                    likes.remove(id)
                }
                self.movieLikes.accept(likes)
            })
            .disposed(by: bag)
        
        movieLikes
            .skip(1)
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .subscribe(onNext: { likes in
                NowPlayingViewModel.saveLikes(likes: likes)
            })
            .disposed(by: bag)
        
        NowPlayingViewModel.loadLikes()
            .asObservable()
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .catchErrorJustReturn(Set<Int>())
            .bind(to: movieLikes)
            .disposed(by: bag)
    }
    
    //data is small so it's justified to save to a file only
    private static func loadLikes() -> Single<Set<Int>> {
        return Single<Set<Int>>.create { single in
            let disposable = Disposables.create()
            
            let url = FileManager
                .default
                .urls(for: .documentDirectory, in: .userDomainMask)[0]
                .appendingPathComponent(NowPlayingViewModel.moviesLikeFileName)
            
            do {
                let data = try Data(contentsOf: url)
                let jsonDecoder = JSONDecoder()
                let likes = try jsonDecoder.decode(Set<Int>.self, from: data)
                
                single(.success(likes))
                return disposable
            } catch {
                single(.error(error))
                return disposable
            }
        }
    }
    
    private static func saveLikes(likes: Set<Int>) {
        let url = FileManager
            .default
            .urls(for: .documentDirectory, in: .userDomainMask)[0]
            .appendingPathComponent(moviesLikeFileName)
        
        let jsonEncoder = JSONEncoder()
        let data = try? jsonEncoder.encode(likes)
        try? data?.write(to: url, options: [.atomic])
    }
    
}
