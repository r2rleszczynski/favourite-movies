//
//  ViewController.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 21/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class NowPlayingViewController: UIViewController {
    
    private var viewModel: NowPlayingViewModel!
    private var navigator: Navigator!
    private var rxReady: Bool = false
    private let bag = DisposeBag()

    private let searchController = UISearchController(searchResultsController: nil)
    private weak var refreshControl: UIRefreshControl!
    private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    
    static func createWith(navigator: Navigator, storyboard: UIStoryboard, viewModel: NowPlayingViewModel) -> NowPlayingViewController {
        let vc = storyboard.instantiateViewController(ofType: NowPlayingViewController.self)
        vc.navigator = navigator
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        viewModel = NowPlayingViewModel()
        
        tableView.estimatedRowHeight = 156
        tableView.rowHeight = 156
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = .black
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        self.activityIndicator = activityIndicator

        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tableView.refreshControl = refreshControl
        self.refreshControl = refreshControl
        
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.autocapitalizationType = .none
        
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
            navigationItem.searchController = searchController
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !rxReady {
           //Needed for circunvent issue:
           //https://github.com/ReactiveX/RxSwift/issues/2081
           //https://github.com/RxSwiftCommunity/RxDataSources/issues/331
            
            rxReady = setupRx()
        }
    }
    
    private func setupRx() -> Bool {
        tableView.rx
            .modelSelected(Movie.self)
            .subscribe(onNext: { [weak self] movie in
                guard let self = self else {
                    return
                }
                
                self.tableView.selectRow(at: nil, animated: true, scrollPosition: .none)
                
                let movie = Observable.just(movie).asDriver(onErrorJustReturn: movie)
                let movieLikes = self.viewModel.movieLikes.asDriver()
                
                self.navigator.show(segue: .movieDetails(movie: movie,
                                                         newMovieLike: self.viewModel.newMovieLike,
                                                         movieLikes: movieLikes)
                    , sender: self)
            })
            .disposed(by: bag)
        
        searchController.searchBar
            .rx.text
            .orEmpty
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .bind(to: viewModel.searchText)
            .disposed(by: bag)

        viewModel.moviesList
            .skip(1)
            .map { $0.results }
            .bind(to: tableView.rx.items(cellIdentifier: "MovieTableViewCell")) { [weak self] (index: Int, movie: Movie, cell: MovieTableViewCell) in
                guard let self = self else {
                    return
                }
                cell.updateUI(movie: movie)

                self.viewModel.movieLikes
                    .map { $0.contains(movie.id) }
                    .subscribe(onNext: { isLiked in
                        cell.likeButton.tintColor = isLiked ? UIColor.systemRed : UIColor.systemGray
                        cell.likeButton.isSelected = isLiked
                    })
                    .disposed(by: cell.bag)
                
                cell.likeButton.rx
                    .controlEvent(.touchUpInside)
                    .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
                    .map { (movie.id, !cell.likeButton.isSelected ) }
                    .bind(to: self.viewModel.newMovieLike)
                    .disposed(by: cell.bag)
            }
            .disposed(by: bag)
        
        refreshControl.rx
            .controlEvent(.valueChanged)
            .bind(to: viewModel.refreshTrigger)
            .disposed(by: bag)
        
        viewModel.activity
            .asObservable()
            .bind(to: refreshControl.rx.isRefreshing)
            .disposed(by: bag)
        
        viewModel.activity
            .asObservable()
            .bind(to: activityIndicator.rx.isAnimating)
            .disposed(by: bag)

        tableView.rx
            .reachedBottom(offset: 44)
            .bind(to: viewModel.nextPageTrigger)
            .disposed(by: bag)
        
        return true
    }

}

extension MovieTableViewCell {
    func updateUI(movie: Movie) {
        title.text = movie.title
        overview.text = movie.overview
        rating.text = String(format: "%.1f", movie.voteAverage)
        if let url = movie.posterUrl {
            poster.kf.indicatorType = .activity
            poster.kf.setImage(with: url, placeholder: UIImage(named: "blank-avatar")) { [weak self] result in
                if case .failure = result, let self = self {
                    self.poster.image = UIImage(named: "no-image-available")
                }
            }
        } else {
            poster.kf.cancelDownloadTask()
            poster.image = UIImage(named: "no-image-available")
        }

    }
}
