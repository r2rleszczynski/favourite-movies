//
//  MovieTableViewCell.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 23/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    var bag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
}
