//
//  MovieDetailsViewController.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 23/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class MovieDetailsViewController: UIViewController {

    private var viewModel: MovieDetailsViewModel!
    private var navigator: Navigator!
    private let bag = DisposeBag()

    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    private weak var likeButton: UIButton!
        
    static func createWith(navigator: Navigator, storyboard: UIStoryboard, viewModel: MovieDetailsViewModel) -> MovieDetailsViewController {
        let vc = storyboard.instantiateViewController(ofType: MovieDetailsViewController.self)
        vc.navigator = navigator
        vc.viewModel = viewModel
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let likeButton = UIButton(type: .custom)
        likeButton.translatesAutoresizingMaskIntoConstraints = false
        likeButton.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
        likeButton.setImage(UIImage(named: "heart"), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: likeButton)
        self.likeButton = likeButton
        
        NSLayoutConstraint.activate([
            likeButton.widthAnchor.constraint(equalToConstant: 32),
            likeButton.heightAnchor.constraint(equalToConstant: 28)
        ])
        
        Observable.combineLatest(viewModel.movieLikes.asObservable(), viewModel.movie.asObservable())
            .map { $0.0.contains($0.1.id) }
            .subscribe(onNext: { isLiked in
                likeButton.tintColor = isLiked ? UIColor.systemRed : UIColor.systemGray
                likeButton.isSelected = isLiked
            })
            .disposed(by: bag)

        likeButton.rx
            .controlEvent(.touchUpInside)
            .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
            .withLatestFrom(viewModel.movie)
            .map { ($0.id, !likeButton.isSelected ) }
            .bind(to: viewModel.newMovieLike)
            .disposed(by: bag)

        viewModel.movie
            .map { $0.releaseYear != nil ? "\($0.title) (\($0.releaseYear!))" : $0.title }
            .drive(titleLabel.rx.text)
            .disposed(by: bag)

        viewModel.movie
            .map { $0.overview }
            .drive(overviewLabel.rx.text)
            .disposed(by: bag)

        viewModel.movie
            .map { String(format: "%.1f", $0.voteAverage) }
            .drive(ratingLabel.rx.text)
            .disposed(by: bag)
        
        viewModel.movie
            .compactMap { $0.bigPosterUrl }
            .asObservable()
            .subscribe(onNext: { [weak posterImageView] url in
                guard let poster = posterImageView else {
                    return
                }
                poster.kf.indicatorType = .activity
                poster.kf.setImage(with: url, placeholder: UIImage(named: "blank-avatar")) { [weak poster] result in
                    if case .failure = result {
                        poster?.image = UIImage(named: "no-image-available")
                    }
                }
            })
            .disposed(by: bag)
        
                

    }
    
}
