//
//  API.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 21/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire
import RxSwiftExt

protocol MovieDBAPIProtocol {
    static func imageConfig() -> Observable<ImageConfiguration>
    
    static func nowPlaying(page: Int?) -> Observable<ListResponse<Movie>>
    static func nowPlaying(language: String?, page: Int?, region: String?) -> Observable<ListResponse<Movie>>
    
    static func searchMovie(query: String, page: Int?) -> Observable<ListResponse<Movie>>
    static func searchMovie(query: String, language: String?, page: Int?, region: String?) -> Observable<ListResponse<Movie>>
}

extension MovieDBAPIProtocol {
    static func nowPlaying(page: Int?) -> Observable<ListResponse<Movie>> {
        return nowPlaying(language: nil, page: page, region: nil)
    }
    
    static func searchMovie(query: String, page: Int?) -> Observable<ListResponse<Movie>> {
        return searchMovie(query: query, language: nil, page: page, region: nil)
    }
}

struct MovieDBAPI: MovieDBAPIProtocol {
    private static let apiKey = "1a9862b3574e4dc3281fe99b17f76582"
    
    private enum Address: String {
        case nowPlaying = "movie/now_playing"
        case searchMovie = "search/movie"
        case configuration = "configuration"
        
        private var baseURL: URL {
            return URL(string: "https://api.themoviedb.org/3/")!
        }

        var url: URL {
            return baseURL.appendingPathComponent(rawValue)
        }
    }
    
    enum Error: Swift.Error {
        case invalidConfigData
    }
    
    private init() {}
            
    static func nowPlaying(language: String?, page: Int?, region: String?) -> Observable<ListResponse<Movie>> {
        var parameters: Parameters = [
            "api_key" : apiKey
        ]
        parameters["language"] = language //if nil it won't be added
        parameters["page"] = page
        parameters["region"] = region
        
        return request(.get, Address.nowPlaying.url, parameters: parameters)
                .validate()
                .decodable(of: ListResponse<Movie>.self, queue: .global(qos: .userInteractive))
    }
    
    static func searchMovie(query: String, language: String?, page: Int?, region: String?) -> Observable<ListResponse<Movie>> {
        var parameters: Parameters = [
            "api_key" : apiKey,
            "query" : query
        ]
        parameters["language"] = language //if nil it won't be added
        parameters["page"] = page
        parameters["region"] = region

        return request(.get, Address.searchMovie.url, parameters: parameters)
                .validate()
                .decodable(of: ListResponse<Movie>.self, queue: .global(qos: .userInteractive))
    }
    
    static func imageConfig() -> Observable<ImageConfiguration> {
        return request(.get, Address.configuration.url, parameters: ["api_key" : apiKey])
            .validate()
            .json()
            .map { json -> ImageConfiguration in
                //manual json parsing because just two values are needed
                if let config = json as? [String: AnyObject],
                    let images = config["images"] as? [String: AnyObject],
                    let imagesUrlString = images["secure_base_url"] as? String,
                    let imagesUrl = URL(string: imagesUrlString),
                    let posterSizes = images["poster_sizes"] as? [String] {
                    
                    return ImageConfiguration(secureBaseUrl: imagesUrl, posterSizes: posterSizes)
                } else {
                    throw Error.invalidConfigData
                }
            }
    }
    
}
