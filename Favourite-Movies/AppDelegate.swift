//
//  AppDelegate.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 21/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let navigator = Navigator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let nc = window!.rootViewController! as! UINavigationController
        navigator.show(segue: .nowPlaying, sender: nc)

        return true
    }


}

