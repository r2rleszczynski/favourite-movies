//
//  Navigator.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 23/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa

class Navigator {
    lazy private var defaultStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    enum Segue {
        case nowPlaying
        case movieDetails(movie: Driver<Movie>, newMovieLike: PublishRelay<(Int, Bool)>, movieLikes: Driver<Set<Int>>)
    }
    
    func show(segue: Segue, sender: UIViewController) {
        switch segue {
        case .nowPlaying:
            let vm = NowPlayingViewModel()
            let target = NowPlayingViewController
                .createWith(navigator: self,
                            storyboard: sender.storyboard ?? defaultStoryboard,
                            viewModel: vm)
            
            show(target: target, sender: sender)
        case .movieDetails(let movie, let newMovieLike, let movieLikes):
            let vm = MovieDetailsViewModel(movie: movie,
                                           newMovieLike: newMovieLike,
                                           movieLikes: movieLikes)
            let target = MovieDetailsViewController
                .createWith(navigator: self,
                            storyboard: sender.storyboard ?? defaultStoryboard,
                            viewModel: vm)
            show(target: target, sender: sender)
        }
    }
    
    private func show(target: UIViewController, sender: UIViewController) {
        if let nav = sender as? UINavigationController {
            nav.pushViewController(target, animated: false)
            return
        }
        
        if let nav = sender.navigationController {
            nav.pushViewController(target, animated: true)
        } else {
            sender.present(target, animated: true, completion: nil)
        }
    }
}
