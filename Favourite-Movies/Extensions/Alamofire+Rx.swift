//
//  Alamofire+Rx.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 21/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Alamofire
import RxAlamofire
import RxSwift

extension ObservableType where Element == DataRequest {
    /// Returns an `Observable` of a serialized Decodable for the current request.
    // optional parameter 'of type' - trick to help compiler and to make code more readable
    public func responseDecodable<T:Decodable>(of type: T.Type = T.self, queue: DispatchQueue = .main, decoder: DataDecoder = JSONDecoder()) -> Observable<(HTTPURLResponse, T)> {
        return flatMap { $0.rx.responseResult(queue: queue, responseSerializer: DecodableResponseSerializer(decoder: decoder)) }
    }
    
    /// Returns an `Observable` of a serialized Decodable for the current request.
    public func decodable<T:Decodable>(of type: T.Type = T.self, queue: DispatchQueue = .main, decoder: DataDecoder = JSONDecoder()) -> Observable<T> {
        return flatMap { return $0.rx.result(queue: queue, responseSerializer: DecodableResponseSerializer(decoder: decoder)) }
    }

}
