//
//  StoryBoard+.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 23/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    func instantiateViewController<T>(ofType type: T.Type) -> T {
        return instantiateViewController(withIdentifier: String(describing: type)) as! T
    }
}
