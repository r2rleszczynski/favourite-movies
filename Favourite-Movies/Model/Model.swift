//
//  Model.swift
//  Favourite-Movies
//
//  Created by Artur Leszczyński on 21/06/2020.
//  Copyright © 2020 Artur Leszczyński. All rights reserved.
//

import Foundation

struct ListResponse<Result: Codable>: Codable {    
    let page: Int
    let results: [Result]
    let totalPages: Int
    let totalResults: Int
    
    enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

extension ListResponse where Result == Movie {
    static var empty: ListResponse<Movie> {
        ListResponse<Movie>(page: 0, results: [], totalPages: 0, totalResults: 0)
    }
    
    var isEmpty: Bool {
        return page == 0 && results.isEmpty && totalPages == 0 && totalPages == 0
    }
}

struct Movie: Codable, Hashable, Equatable {
    let id: Int //tmdb ids are in range of 32bit so its safe to use Int, considering iPhone5 and 5c has 32 bit architecture
    let posterPath: String?
    let overview: String
    let releaseDate: String?
    let genreIds: [Int64]
    let title: String
    let originalTitle: String
    let voteAverage: Float
    
    // MARK: local values
    var posterUrl: URL?
    var bigPosterUrl: URL?
    var isLiked: Bool?
    var releaseYear: String? {
        return releaseDate != nil ? String(releaseDate!.prefix(4)) : nil
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case posterPath = "poster_path"
        case overview
        case releaseDate = "release_date"
        case genreIds = "genre_ids"
        case title
        case originalTitle = "original_title"
        case voteAverage = "vote_average"
    }
}

struct ImageConfiguration {
    let secureBaseUrl: URL
    let posterSizes: [String]
    
    // fallback config
    static var `default`: ImageConfiguration {
        return ImageConfiguration(secureBaseUrl: URL(string: "https://image.tmdb.org/t/p/")!, posterSizes: ["w92", "w154", "w185", "w342", "w500", "w780", "original"])
    }
}
